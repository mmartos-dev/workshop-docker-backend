'use strict';
module.exports = function(app) {
    const todoList = require('../controllers/todoListController');

    // todoList Routes
    app.route('/tasks')
        .get(todoList.getAllTasks)
        .post(todoList.createTask);

    app.route('/tasks/:taskId')
        .get(todoList.getTask)
        .put(todoList.updateTask)
        .delete(todoList.removeTask);
};
