'use-strict';

module.exports = {
    mapTask: (task) => {
        return {
            id: task._id,
            name: task.name,
            creationDate: task.creationDate,
            status: task.status
        };
    }
}