'use strict';

const mongoose = require('mongoose');
const mappers = require('./modelMapper.js');
const Task = mongoose.model('Tasks');

exports.getAllTasks = (req, res) => {
    Task.find().exec()
        .then((tasks) => res.json(tasks.map(mappers.mapTask)))
        .catch(res.send);
};

exports.createTask = (req, res) => {
    let new_task = new Task(req.body);
    new_task.save()
        .then((task) => res.json(mappers.mapTask(task)))
        .catch(res.send);
};

exports.getTask = (req, res) => {
    Task.findById(req.params.taskId).exec()
        .then((task) => res.json(mappers.mapTask(task)))
        .catch(() => res.json({error: 'Requested task not found'}));
};

exports.updateTask = (req, res) => {
    console.log('update');
    console.log('taskId -> ' + req.params.taskId);
    console.log('body -> ' + JSON.stringify(req.body));
    Task.findOneAndUpdate({_id: req.params.taskId}, req.body, {new: true}).exec()
        .then((task) => {
            console.log('then closure');
            if (task !== null) {
                res.json(mappers.mapTask(task));
            } else {
                res.json({error: 'Requested task not found'});
            }
        })
        .catch(() => res.json({error: 'Requested task not found'}));
};

exports.removeTask = (req, res) => {
    Task.findOneAndDelete({_id: req.params.taskId}).exec()
        .then((task) => {
            if (task !== null) {
                res.json({ message: 'Task successfully deleted'})
            } else {
                res.json({error: 'Requested task not found'});
            }
        })
        .catch(() => res.json({error: 'Requested task not found'}));
};