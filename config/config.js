'use strict';
const API_PORT = process.env.API_PORT || 3000
const MONGODB_SERVER = process.env.MONGODB_SERVER || "localhost"

module.exports = {
    'api': {
        'port': API_PORT
    },
    'mongodb': {
        'server': MONGODB_SERVER
    }
}